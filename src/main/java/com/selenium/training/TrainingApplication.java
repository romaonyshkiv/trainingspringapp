package com.selenium.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 *
 *
 * to run this app type in terminal: mvn spring-boot:run
 *
 */

@SpringBootApplication
public class TrainingApplication {

	public static void main(String[] args) {

		SpringApplication.run(TrainingApplication.class, args);

	}

}

