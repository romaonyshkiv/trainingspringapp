package com.selenium.training;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class UserDTO {

    private String firstName;
    private String lastName;
    private int age;


}
