package com.selenium.training;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @PostMapping(name = "/create")
    public ResponseEntity create(@RequestBody UserDTO userDTO) {

        HttpStatus status = HttpStatus.BAD_REQUEST;

        if (userDTO.getFirstName() != null & userDTO.getLastName() != null & userDTO.getAge() >= 0)
            status = HttpStatus.CREATED;

        return ResponseEntity.status(status).build();
    }

    @GetMapping(name = "/")
    public String index() {

        return "Welcome to homepage " + System.getProperty("user.name").toUpperCase();
    }
}
